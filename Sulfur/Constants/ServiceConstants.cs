﻿namespace Sulfur.Constants
{
    //enums to be used by the UrlFileDownload service
    public enum UrlFileDlEnums
    {
        WebUrlIsNotValid,
        FileIncorrectFormat,
        Success
    }

    public static class ServiceConstants
    {
        //Constants to be used for the services

        //The auth password for the header token
        public const string AuthTokenPassword = "TEST";

        //used by the UrlFileDownload service when guessing a mime type
        public const string TorrentMimeType = "application/x-bittorrent";
    }
}
