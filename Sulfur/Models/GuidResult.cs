﻿using Newtonsoft.Json;

namespace Sulfur.Models
{
    //On receiving a url payload, we return the guid result in this model
    public class GuidResult
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("success")]
        public string Success { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
