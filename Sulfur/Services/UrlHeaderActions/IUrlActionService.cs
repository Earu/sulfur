﻿using Sulfur.Constants;
using Sulfur.Models;

namespace Sulfur.Services.UrlHeaderActions
{
    public interface IUrlActionService
    {
        bool SuccessfulMatchOnHeaderToken(string headerToken);

        GuidResult GenerateGuidPayload(bool ableToProcessUrl, UrlFileDlEnums urlFileDlEnum);
    }
}
