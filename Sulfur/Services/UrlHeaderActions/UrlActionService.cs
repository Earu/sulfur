﻿using Sulfur.Constants;
using Sulfur.Models;
using System;

namespace Sulfur.Services.UrlHeaderActions
{
    public class UrlActionService : IUrlActionService
    {
        //Checks whether the auth header token matches our predefined value
        public bool SuccessfulMatchOnHeaderToken(string headerToken)
            => headerToken.Equals(ServiceConstants.AuthTokenPassword);

        //Creates an appropriate message and boolean, based on if the url was able to be processed successfully
        public GuidResult GenerateGuidPayload(bool ableToProcess, UrlFileDlEnums resultEnum)
        {
            //Whats ableToProcess variable for ?

            string errormessage = string.Empty;

            Guid code = Guid.NewGuid();
            string guid = code.ToString();

            GuidResult responsePayload = new GuidResult();

            switch (resultEnum)
            {
                case UrlFileDlEnums.FileIncorrectFormat:
                    errormessage = Resource.FileIncorrectFormat;
                    responsePayload.Success = "false";
                    responsePayload.Error = errormessage;
                    return responsePayload;

                case UrlFileDlEnums.WebUrlIsNotValid:
                    errormessage = Resource.InvalidWebUrl;
                    responsePayload.Success = "false";
                    responsePayload.Error = errormessage;
                    return responsePayload;

                default:
                    responsePayload.Id = guid;
                    responsePayload.Success = "true";
                    responsePayload.Error = errormessage;
                    return responsePayload;
            }
        }
    }
}
