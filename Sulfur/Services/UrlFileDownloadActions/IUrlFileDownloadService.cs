﻿using Sulfur.Constants;

namespace Sulfur.Services.UrlFileDownloadActions
{
    public interface IUrlFileDownloadService
    {
        (bool, UrlFileDlEnums) ProcessUrl(string url);
    }
}
