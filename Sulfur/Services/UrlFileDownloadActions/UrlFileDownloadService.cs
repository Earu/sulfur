﻿using HeyRed.Mime;
using Sulfur.Constants;
using System.Net;

namespace Sulfur.Services.UrlFileDownloadActions
{
    public class UrlFileDownloadService : IUrlFileDownloadService
    {
        //Attempts to download the file at the url
        //Returns a bool and enum signifying if successfull
        //false, WEBURLISNOTVALID - The url is not valid, could be a 500 or 404 and it was unable to download
        //false, FILEINCORRECTFORMAT - The url is valid but the file at the url is not a torrent file
        //true, SUCCESS - Url is valid, file is a torrent file and was able to download.
        public (bool, UrlFileDlEnums) ProcessUrl(string url)
        {
            if (!ValidUrl(url))
                return (false, UrlFileDlEnums.WebUrlIsNotValid);
            
            //Retrieve files as bytes, to be used by mime guesser
            byte[] downloadFile = DownloadFileFromUrl(url);

            if (IsTorrentFile(downloadFile))
                return (true, UrlFileDlEnums.Success);

            return (false, UrlFileDlEnums.FileIncorrectFormat);
        }

        //Returns whether the url is valid
        private bool ValidUrl(string url)
        {
            HttpWebResponse response = null;
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "HEAD";

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch
            {
                /* A WebException will be thrown if the status of the response is not `200 OK` */
                return false;
            }
            finally
            {
                // Don't forget to close your response.
                response?.Close();
            }

            return true;
        }

        //Checks if the file is a torrent file
        private bool IsTorrentFile(byte[] torrentFileAsBytes)
        {
            //Attempts to guess and return the file type
            string mimeType = MimeGuesser.GuessMimeType(torrentFileAsBytes); //=> image/jpeg

            return mimeType.Equals(ServiceConstants.TorrentMimeType);
        }

        //Attempts to download the file into a byte array
        private byte[] DownloadFileFromUrl(string url)
        {
            byte[] torrentFileAsByteArray;
            using (var webClient = new WebClient())
            {
                torrentFileAsByteArray = webClient.DownloadData(url);
                return torrentFileAsByteArray;
            }
        }
    }
}
