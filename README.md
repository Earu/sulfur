[![pipeline status](https://gitlab.com/Earu/sulfur/badges/master/pipeline.svg)](https://gitlab.com/Earu/sulfur/commits/master)

# Energize.Sulfur
A stream forwarding node that creates media streams from complex formats
