﻿using Moq;
using Sulfur;
using Sulfur.Constants;
using Sulfur.Models;
using Sulfur.Services.UrlHeaderActions;
using Xunit;

namespace SulfurXunitTests
{
    public class UrlActionServiceTests
    {
        private readonly IUrlActionService actionService;
        public UrlActionServiceTests()
        {
            var mock = new Mock<UrlActionService>();
            actionService = mock.Object;
        }

        //Just checking there's a match on the received auth token 
        [Fact]
        public void CheckAuthTokenMethodIsSuccessful()
        {    
            Assert.True(actionService.SuccessfulMatchOnHeaderToken(ServiceConstants.AuthTokenPassword));
        }

        //When file has an incorrect format a certain guid should be generated
        [Fact]
        public void GenerateGuidWhenFileIsIncorrectFormat()
        {
            GuidResult resultPayload = actionService.GenerateGuidPayload(false, UrlFileDlEnums.FileIncorrectFormat);

            Assert.True(string.IsNullOrEmpty(resultPayload.Id));
            Assert.False(bool.Parse(resultPayload.Success));
            Assert.Equal(resultPayload.Error, Resource.FileIncorrectFormat);
        }

        //When url is not valid a certain guid should be generated
        [Fact]
        public void GenerateGuidWhenUrlIsInvalid()
        {
            GuidResult resultPayload = actionService.GenerateGuidPayload(false, UrlFileDlEnums.WebUrlIsNotValid);

            Assert.True(string.IsNullOrEmpty(resultPayload.Id));
            Assert.False(bool.Parse(resultPayload.Success));
            Assert.Equal(resultPayload.Error, Resource.InvalidWebUrl);
        }

        //When url is valid and torrent file exists
        [Fact]
        public void GenerateGuidWhenUrlIsValidAndTorrentFileExists()
        {
            GuidResult resultPayload = actionService.GenerateGuidPayload(true, UrlFileDlEnums.Success);

            Assert.False(string.IsNullOrEmpty(resultPayload.Id));
            Assert.True(bool.Parse(resultPayload.Success));
            Assert.True(string.IsNullOrEmpty(resultPayload.Error));
        }
    }
}
